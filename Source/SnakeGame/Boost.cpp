
#include "Boost.h"
#include "SnakeBase.h"

// Sets default values
ABoost::ABoost()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bUpOrDown = 1;
}

// Called when the game starts or when spawned
void ABoost::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(2);
}

// Called every frame
void ABoost::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	// ��������� ����������� �������� bUpOrDown
	if (bUpOrDown)
	{
		bUpOrDown = 0;
		
	}
	else
	{
		bUpOrDown = 1;
	}
}

// � ����������� �� �������� bUpOrDown �� ���� �������� ������, ���� ��������� �
void ABoost::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			if (bUpOrDown)
			{
				Snake->SetActorTickInterval(Snake->MovementSpeed * 2);
				this->Destroy();
			}
			else
			{
				Snake->SetActorTickInterval(Snake->MovementSpeed * 0.5);
				this->Destroy();
			}
		}
	}
}

