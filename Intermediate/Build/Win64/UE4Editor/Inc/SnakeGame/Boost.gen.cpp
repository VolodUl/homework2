// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Boost.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBoost() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ABoost_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABoost();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ABoost::StaticRegisterNativesABoost()
	{
	}
	UClass* Z_Construct_UClass_ABoost_NoRegister()
	{
		return ABoost::StaticClass();
	}
	struct Z_Construct_UClass_ABoost_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUpOrDown_MetaData[];
#endif
		static void NewProp_bUpOrDown_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUpOrDown;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABoost_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Boost.h" },
		{ "ModuleRelativePath", "Boost.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABoost_Statics::NewProp_bUpOrDown_MetaData[] = {
		{ "Category", "Boost" },
		{ "ModuleRelativePath", "Boost.h" },
	};
#endif
	void Z_Construct_UClass_ABoost_Statics::NewProp_bUpOrDown_SetBit(void* Obj)
	{
		((ABoost*)Obj)->bUpOrDown = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ABoost_Statics::NewProp_bUpOrDown = { "bUpOrDown", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ABoost), &Z_Construct_UClass_ABoost_Statics::NewProp_bUpOrDown_SetBit, METADATA_PARAMS(Z_Construct_UClass_ABoost_Statics::NewProp_bUpOrDown_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABoost_Statics::NewProp_bUpOrDown_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABoost_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABoost_Statics::NewProp_bUpOrDown,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ABoost_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ABoost, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABoost_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABoost>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABoost_Statics::ClassParams = {
		&ABoost::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ABoost_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ABoost_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABoost_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABoost_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABoost()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABoost_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABoost, 1412181473);
	template<> SNAKEGAME_API UClass* StaticClass<ABoost>()
	{
		return ABoost::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABoost(Z_Construct_UClass_ABoost, &ABoost::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ABoost"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABoost);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
