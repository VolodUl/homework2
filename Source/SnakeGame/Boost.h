// ������, ������� ����� ������� ���� ����� �������� ������ 2 �������:
// ���� ����������, ���� ��������

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Boost.generated.h"

UCLASS()
class SNAKEGAME_API ABoost : public AActor, public IInteractable
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintReadOnly)
	bool bUpOrDown;

public:	
	// Sets default values for this actor's properties
	ABoost();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Interact(AActor* Interactor, bool bIsHead) override;
};
